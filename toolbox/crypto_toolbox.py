import base64
from Crypto.Cipher import AES

ascii_list = [chr(i) for i in range(256)]
letter_frequencies_en = {
    'a': 0.0651738, 'b': 0.0124248, 'c': 0.0217339,
    'd': 0.0349835, 'e': 0.1041442, 'f': 0.0197881,
    'g': 0.0158610, 'h': 0.0492888, 'i': 0.0558094,
    'j': 0.0009033, 'k': 0.0050529, 'l': 0.0331490,
    'm': 0.0202124, 'n': 0.0564513, 'o': 0.0596302,
    'p': 0.0137645, 'q': 0.0008606, 'r': 0.0497563,
    's': 0.0515760, 't': 0.0729357, 'u': 0.0225134,
    'v': 0.0082903, 'w': 0.0171272, 'x': 0.0013692,
    'y': 0.0145984, 'z': 0.0007836, ' ': 0.1918182}


def hex_to_bytes(hex_string):
    """Convert a hex string into bytes"""
    return bytes.fromhex(hex_string)


def bytes_to_hex(byte_value):
    """Convert bytes into hexadecimal"""
    return byte_value.hex()


def bytes_to_base64(byte_value):
    """Convert bytes into a base64 encoded string"""
    return base64.b64encode(byte_value).decode('utf-8')


def base64_to_bytes(base64_value):
    """Convert bytes into a base64 encoded string"""
    return base64.b64decode(base64_value)


def bitwise_XOR(byte_value1, byte_value2):
    """Bitwise XOR between to byte values"""
    return bytes(x ^ y for x, y in zip(byte_value1, byte_value2))


def singlechar_XOR(byte_value, byte_singlechar):
    """Bitwise XOR between byte values and a single character byte"""
    return bitwise_XOR(byte_value, byte_singlechar * len(byte_value))


def repeatkey_XOR(byte_value, byte_key):
    """Bitwise XOR between byte values and a repeated multi character key in bytes"""
    return bitwise_XOR(byte_value, (byte_key * len(byte_value))[:len(byte_value)])


def letter_frequency_scoring(byte_message, scoring_dict=None):
    """Score byte encoded message by summation of its letter's score given in a scoring dictionary (default is letter
    probability in the english language)"""
    if not scoring_dict:
        scoring_dict = letter_frequencies_en
    score = 0
    for k in byte_message:
        try:
            score += scoring_dict[chr(k).lower()]
        except KeyError:
            pass
    return score


def hamming_distance(byte_value1, byte_value2, byteorder='little'):
    """Calculate the Hamming distance (number of differing bits) between to byte values"""
    return bin(int.from_bytes(bitwise_XOR(byte_value1, byte_value2), byteorder=byteorder)).count('1')


def bruteforce_keysize(byte_value, maximum_key_size, number_of_averages=1):
    """Bruteforce estimate the keysize used for an encrypted message using the Hamming distance as a measure. The
    higher the averaging number the more Hamming distances will be calculated for different sets of the message and
    the more reliable the result gets."""
    hdistances = []
    maximum_key_size += 1
    try:
        byte_value[number_of_averages * maximum_key_size * 2 + maximum_key_size * 2]
    except IndexError:
        print("Number of Averages multiplied by key size exceeds the length of the input data. Please shorten either!")
        raise

    for k in range(2, maximum_key_size):
        hdistance = 0
        for l in range(number_of_averages):
            hdistance += hamming_distance(byte_value[l * k * 2:l * k * 2 + k],
                                          byte_value[l * k * 2 + k:l * k * 2 + k * 2]) / k
        hdistances.append((hdistance / number_of_averages, k))
    return sorted(hdistances)


def bruteforce_singlecharkey(byte_message, key_list=None, scoring_dict=None):
    """Bruteforce a single character key used to encrypt the message by letter frequency analysis based on a scoring
    dictionary. """
    if not key_list:
        key_list = ascii_list
    message_list = []
    score_list = []
    for k in key_list:
        message_list.append(singlechar_XOR(byte_message, k.encode()))
        score_list.append(letter_frequency_scoring(message_list[-1], scoring_dict))
    return list(sorted(zip(score_list, message_list, key_list)))


def bruteforce_repeating_key(byte_value, key_size, scoring_dict=None):
    """Bruteforce a repeating key of multiple characters that has been used to encrypt a message based on letter
    frequency analysis based on a scoring dictionary. """
    key_blocks = [bytearray(ele) for ele in [b'']*key_size]
    _ = [key_blocks[k % key_size].append(byte_value[k]) for k in range(len(byte_value))]
    key = ''
    for k in key_blocks:
        key += bruteforce_singlecharkey(bytes(k), scoring_dict)[-1][-1]
    return key


def decrypt_AES_ECB(key, byte_value):
    """Decrypt a message encrypted using AES in ECB mode with a given key in bytes."""
    cipher = AES.new(key, AES.MODE_ECB)
    return cipher.decrypt(byte_value)


def count_block_repetitions(byte_value, key_size):
    """Count the number of repeating byte blocks for a given key size."""
    byte_key_blocks = [byte_value[l:l + key_size] for l in range(0, len(byte_value), key_size)]
    return len(byte_key_blocks) - len(set(byte_key_blocks))

def is_AES_ECB_encrypted(byte_value):
    """Detect whether ciphertext was AES-ECB encrypted by checking for 16 byte block repetitions. This is not
    deterministic: lack of data patterns would lead to false negatives and any data pattern, e.g. in unencrypted
    data, would lead to false positives. """
    return bool(count_block_repetitions(byte_value, key_size=16))
