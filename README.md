# cryptopals

Solutions to the [cryptopals challenges](https://cryptopals.com/) in python.

## Intro

The cryptopals challenges are a set of 48 cryptographic problems for hands-on learning of cryptography. The challenges require a programmatic solution to a given cryptographic exercise. In this project, python is used as a programming language.

Have fun hacking away to the lyrics of [Ice Ice Baby](https://www.youtube.com/watch?v=rog8ou-ZepE)...

## Solutions

### Set 1: Basics

#### 1. [Convert hex to base64](https://cryptopals.com/sets/1/challenges/1)

The first challenge is rather simple, but instructive by forcing you to think about data types and encoding.

An essential learning is to always work raw bytes.

We define the input as a hexadecimal string that we convert to bytes and then to base64 via the base64 module in python.

["I'm killing your brain like a poisonous mushroom"](./set1_challenge1.py)

#### 2. [Fixed XOR](https://cryptopals.com/sets/1/challenges/2)

The XOR or exclusive-or is a logical operation that is FALSE if both inputs are the same ([0,0] or [1,1]) and TRUE if both inputs are different ([0,1] or [1,0]). Since it is reversible in contrast to other logical operators (AND, OR), it is used as a very basic encryption/decryption function by applying it bitwise on message and key (thus encrypting [scrambling each character of] the message with the key). In fact, XOR is its own inverse function and thus bitwise XOR of the encrypted message with the same key yields the plaintext message. That makes it a very simple symmetric encryption method referred to as [XOR cipher](https://en.wikipedia.org/wiki/XOR_cipher).

The exercise can be understood as encryption or decryption of a message with the XOR cipher using a same length key.

We use the XOR operator "^" of python and iterate over the bytes.

["the kid don't play"](./set1_challenge2.py)

#### 3. [Single-byte XOR cipher](https://cryptopals.com/sets/1/challenges/3)

This challenge demonstrates how the XOR cipher with a repeating key (shorter than the message and in our case only 1 character) is vulnerable to [frequency analysis](https://en.wikipedia.org/wiki/Frequency_analysis). Essentially, we are brute-forcing the encryption key by testing all ASCII characters as keys and scoring the "decrypted" message via comparison of its character occurrences versus their probability in the English language.

Technically, the string is converted to bytes and XORed with each ASCII character based on the bitwise XOR from solution 2. For scoring, we iterate over each character of the "decrypted" message and sum up their probability in the English language ('0' for unused characters).

**Note:** Choosing a letter frequency source without space changes the outcome. So pick your source wisely considering what language/type of decrypted message you expect.

["Cooking MC's like a pound of bacon"](./set1_challenge3.py)

#### 4. [Detect single-character XOR](https://cryptopals.com/sets/1/challenges/4)

For the fourth challenge, we only extend problem 3 to a list of messages one of which is encrypted with a single character. This can be seen as either finding an encrypted string in a set of data or stepwise decryption in case of non-constant or multiple keys, e.g. a database with separate key per entry.

["Now that the party is jumping"](./set1_challenge4.py)

#### 5. [Implement repeating-key XOR](https://cryptopals.com/sets/1/challenges/5)

Here, we encrypt a message with a constant short multi-character key. Only thing to keep an eye on maybe that the message may not be an integer multiple of the full key. Just need to make sure to apply your key byte-wise.

["Burning 'em, if you ain't quick and nimble I go crazy when I hear a cymbal"](./set1_challenge5.py)

#### 6. [Break repeating-key XOR](https://cryptopals.com/sets/1/challenges/6)

The major conceptual learning of this challenge is how to estimate an a priori unknown key size of an encrypted message by employing the Hamming distance. The Hamming distance is the number of differing bits between two strings. For two random bytes (set of 8 bits), you would expect the Hamming distance to be 4. However, in case your entropy is lower (as is for example the case for byte-encoded ASCII characters of a text in English language) the average Hamming distance would be smaller. A repeating-key XOR encryption will not change the Hamming distance when comparing matchings sets for the right key size. Choosing sets of the wrong key size will, however, increase the randomness as it does not reflect the structured lower entropy English language outcome anymore. This is described in more detail in a comment on [StackExchange](https://crypto.stackexchange.com/a/48295).

A second learning of this challenge is how to brute-force a repeating key of known length that was used to encrypt a message: Rearrange the encrypted message into groups of bytes for each key character and proceed for each group as we did for the single-byte XOR cipher using frequency analysis. Essentially, brute-force the key letter-by-letter.

["Terminator X: Bring the noise"](./set1_challenge6.py)

#### 7. [AES in ECB mode](https://cryptopals.com/sets/1/challenges/7)

In the previous challenges, we have explored the basics of block cipher encryption using the XOR algorithm. We have seen how blocks of data can be encrypted block-wise by a symmetric repeating character or multi-character key and how  this elementary implementation is vulnerable to bruteforce attacks using, e.g., letter frequency analysis. This simple block-wise encryption with the same key is referred to as Electronic Codebook (ECB) mode and can also be implemented in more advanced ciphers than the XOR cipher. In this challenge, we explore the Advanced Encryption Standard (AES) that is of central importance for modern cryptography. 

We start by implementing AES decryption in ECB mode. In a first solution, we will use the Openssl library and write an implementation in Python in a second step.

##### a) Openssl library

First create the hex value of the password since Openssl does not accept spaces in passwords.

```bash
printf "YELLOW SUBMARINE" | xxd
```

Then use this hash to decrypt the encrypted file:

```bash
openssl aes-128-ecb -d -a -K 59454c4c4f57205355424d4152494e45 -in encrypted.txt -out decrypted.txt
```

The decrypted text can be read [here](./set1challenge7_decrypted.txt).

##### b) Python implementation

To implement the same in Python, we can just use the Package PyCryptodome which offers many low-level cryptographic primitives. Decryption of the encrypted text in AES-ECB with a given key is then achieved by the following ansatz:

```python
from Crypto.Cipher import AES

cipher = AES.new(key, AES.MODE_ECB)
decrypted = cipher.decrypt(encrypted)
```

Make sure to work in bytes for both, encrypted text and key.

**Note:** Even the PyCryptodome package gives a warning for AES-ECB as "The ECB mode should not be used because it is [semantically insecure](https://en.wikipedia.org/wiki/Semantic_security). For one, it exposes correlation between blocks."

["Play that funky music"](./set1_challenge7.py)

#### 8. [Detect AES in ECB mode](https://cryptopals.com/sets/1/challenges/8)

In this challenge, we use the fact that AES-ECB is stateless and deterministic and consequently creates the same cipher text for the same 16 byte plaintext block. The problem has some similarity to challenge 6 with the Hamming distance, as we are exploiting a reduced entropy of 16 byte blocks for AES-ECB encrypted ciphertexts. While the Hamming distance works as a solution, it is more straight forward to just count the number of repeating blocks in each ciphertext. The solution however relies on repetitions in the plaintext. Such a problem with data patterns is specifically high for [images with areas of same color](https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation#Electronic_Codebook_(ECB)).

[Solution](./set1_challenge8.py)



