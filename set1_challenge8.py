#!/usr/bin/python3

import os
import toolbox.crypto_toolbox as ctools


def count_block_repetitions(byte_value, key_size):
    byte_key_blocks = [byte_value[l:l + key_size] for l in range(0, len(byte_value), key_size)]
    return len(byte_key_blocks) - len(set(byte_key_blocks))


def main(hex_strings, key_size):
    result = []
    for k, hex_string in enumerate(hex_strings):
        byte_value = ctools.hex_to_bytes(hex_string)
        result.append((count_block_repetitions(byte_value, key_size), k))
    return sorted(result, reverse=True)


if __name__ == '__main__':
    key_size = 16

    # read file
    my_path = os.path.abspath(os.path.dirname(__file__))
    path = os.path.join(my_path, "supporting_materials/set1challenge8_encrypted.txt")
    with open(path) as f:
        hex_strings = [k.strip() for k in f.readlines()]

    result = main(hex_strings, key_size)
    if bool(result[0][0]):
        print("Set 1 Challenge 7 solved!")
        print('\nThe highest number of repetitions is %d for cipher text %d' % (result[0]))
        print('The second highest number of repetitions is %d for cipher text %d' % (result[1]))
