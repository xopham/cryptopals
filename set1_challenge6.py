#!/usr/bin/python3

import os
import toolbox.crypto_toolbox as ctools


def hamming_distance(byte_value1, byte_value2):
    return bin(int.from_bytes(ctools.bitwise_XOR(byte_value1, byte_value2), byteorder='little')).count('1')


def bruteforce_keysize(byte_value, maximum_key_size, number_of_averages=1):
    hdistances = []
    maximum_key_size += 1
    try:
        byte_value[number_of_averages * maximum_key_size * 2 + maximum_key_size * 2]
    except IndexError:
        print("Number of Averages multiplied by key size exceeds the length of the input data. Please shorten either!")
        raise

    for k in range(2, maximum_key_size):
        hdistance = 0
        for l in range(number_of_averages):
            hdistance += hamming_distance(byte_value[l * k * 2:l * k * 2 + k],
                                          byte_value[l * k * 2 + k:l * k * 2 + k * 2]) / k
        hdistances.append((hdistance / number_of_averages, k))
    return sorted(hdistances)


def bruteforce_repeating_key(byte_value, key_size):
    key_blocks = [bytearray(ele) for ele in [b''] * key_size]
    _ = [key_blocks[k % key_size].append(byte_value[k]) for k in range(len(byte_value))]
    key = ''
    for k in key_blocks:
        key += ctools.bruteforce_singlecharkey(bytes(k))[-1][-1]
    return key


def main(base64_strings, maximum_key_size, number_of_averages):
    byte_value = ctools.base64_to_bytes(''.join(base64_strings))

    # extract key size
    sorted_hdistances = bruteforce_keysize(byte_value, maximum_key_size, number_of_averages)
    key_size = sorted_hdistances[0][1]

    key = bruteforce_repeating_key(byte_value, key_size)
    return key, key_size, ctools.repeatkey_XOR(byte_value, key.encode())


if __name__ == '__main__':
    ## Test Case for Hamming Distance function
    test_string1 = "this is a test"
    test_string2 = "wokka wokka!!!"

    print('Hamming distance between "%s" and "%s" is: %d'
          % (test_string1, test_string2, hamming_distance(test_string1.encode(), test_string2.encode())))

    ## Main solution
    max_key_size = 40
    n_of_averages = 30

    # read file
    my_path = os.path.abspath(os.path.dirname(__file__))
    path = os.path.join(my_path, "supporting_materials/set1challenge6_encrypted.txt")
    with open(path) as f:
        base64_strings = [k.strip() for k in f.readlines()]

    result = main(base64_strings, max_key_size, n_of_averages)
    print("Set 1 Challenge 6 solved! Well if the following makes sense...")
    print('\nThe key "%s" with a deduced length of %d was found and yields: %s' % result)
