#!/usr/bin/python3

import os
import toolbox.crypto_toolbox as ctools
from Crypto.Cipher import AES


def decrypt_AES_ECB(key, byte_value):
    cipher = AES.new(key, AES.MODE_ECB)
    return cipher.decrypt(byte_value)


def main(base64_string_list, key):
    byte_value = ctools.base64_to_bytes(''.join(base64_string_list))
    return decrypt_AES_ECB(key, byte_value)


if __name__ == '__main__':
    encryption_key = b'YELLOW SUBMARINE'

    # read file
    my_path = os.path.abspath(os.path.dirname(__file__))
    path = os.path.join(my_path, "supporting_materials/set1challenge7_encrypted.txt")
    with open(path) as f:
        base64_strings = [k.strip() for k in f.readlines()]

    result = main(base64_strings, encryption_key)
    print("Set 1 Challenge 7 solved! Well if the following makes sense...")
    print('\nThe key "%s" yields: %s' % (encryption_key.decode(), result.decode()))
