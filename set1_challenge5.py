#!/usr/bin/python3

import toolbox.crypto_toolbox as ctools


def repeatkey_XOR(byte_value, byte_key):
    return ctools.bitwise_XOR(byte_value, (byte_key * len(byte_value))[:len(byte_value)])


def main(message_string, key_string):
    return ctools.bytes_to_hex(repeatkey_XOR(message_string.encode(), key_string.encode()))


if __name__ == '__main__':
    input_string = "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal"
    key = 'ICE'

    output_hex_string = '0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e' \
                        '2c652a3124333a653e2b2027630c692b20283165286326302e27282f'

    if main(input_string, key) == output_hex_string:
        print("Set 1 Challenge 1 solved!")
        print('\n' + input_string)
    else:
        print("Wrong solution.")
