#!/usr/bin/python3

import toolbox.crypto_toolbox as ctools
import os.path


def main(hex_strings):

    all_lists = []
    for hex_string in hex_strings:
        byte_value = ctools.hex_to_bytes(hex_string)
        sorted_list_hexstring = ctools.bruteforce_singlecharkey(byte_value)
        all_lists.extend(sorted_list_hexstring)
    sorted_list = sorted(all_lists)

    [print(m) for m in sorted_list[-10:]]

    return sorted_list[-1]


if __name__ == '__main__':
    # read file
    my_path = os.path.abspath(os.path.dirname(__file__))
    path = os.path.join(my_path, "supporting_materials/set1challenge4_strings.txt")
    with open(path) as f:
        hex_strings = [k.strip() for k in f.readlines()]

    solution = main(hex_strings)
    print("\nFor Set 1 Challenge 4, the best guess for the key is '%s' with a score of %f and a resulting message:"
          " %s" % (solution[2], solution[0], solution[1]))
    print('\n' + str(solution[1]))
