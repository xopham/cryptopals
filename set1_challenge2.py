#!/usr/bin/python3

import toolbox.crypto_toolbox as ctools


def bitwise_XOR(byte_value1, byte_value2):
    return bytes(x ^ y for x, y in zip(byte_value1, byte_value2))


def main(hex_string1, hex_string2):
    if len(hex_string1) != len(hex_string2):
        print("Hex values have different lengths.")
        return False
    return bitwise_XOR(ctools.hex_to_bytes(hex_string1), ctools.hex_to_bytes(hex_string2)).hex()


if __name__ == '__main__':
    input_hex_string1 = '1c0111001f010100061a024b53535009181c'
    input_hex_string2 = '686974207468652062756c6c277320657965'
    output_hex_string = '746865206b696420646f6e277420706c6179'

    if main(input_hex_string1, input_hex_string2) and main(input_hex_string1, input_hex_string2) == output_hex_string:
        print("Set 1 Challenge 2 solved!")
        print('\n' + str(ctools.hex_to_bytes(main(input_hex_string1, input_hex_string2))))
    else:
        print("Wrong solution.")
