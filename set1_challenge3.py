#!/usr/bin/python3

import toolbox.crypto_toolbox as ctools

ascii_list = [chr(i) for i in range(256)]
letter_frequencies_en = {
    'a': 0.0651738, 'b': 0.0124248, 'c': 0.0217339,
    'd': 0.0349835, 'e': 0.1041442, 'f': 0.0197881,
    'g': 0.0158610, 'h': 0.0492888, 'i': 0.0558094,
    'j': 0.0009033, 'k': 0.0050529, 'l': 0.0331490,
    'm': 0.0202124, 'n': 0.0564513, 'o': 0.0596302,
    'p': 0.0137645, 'q': 0.0008606, 'r': 0.0497563,
    's': 0.0515760, 't': 0.0729357, 'u': 0.0225134,
    'v': 0.0082903, 'w': 0.0171272, 'x': 0.0013692,
    'y': 0.0145984, 'z': 0.0007836, ' ': 0.1918182}


def bitwise_XOR(byte_value1, byte_value2):
    return bytes(x ^ y for x, y in zip(byte_value1, byte_value2))


def singlechar_XOR(byte_value, byte_char):
    return ctools.bitwise_XOR(byte_value, byte_char * len(byte_value))


def letter_frequency_scoring(byte_message, scoring_dict=None):
    if not scoring_dict:
        scoring_dict = letter_frequencies_en
    score = 0
    for k in byte_message:
        try:
            score += scoring_dict[chr(k).lower()]
        except KeyError:
            pass
    return score


def bruteforce_singlecharkey(byte_message, key_list=None):
    if not key_list:
        key_list = ascii_list
    message_list = []
    score_list = []
    for k in key_list:
        message_list.append(singlechar_XOR(byte_message, k.encode()))
        score_list.append(letter_frequency_scoring(message_list[-1]))
    return list(sorted(zip(score_list, message_list, key_list)))


def main(hex_string):
    byte_value = ctools.hex_to_bytes(hex_string)
    sorted_list = bruteforce_singlecharkey(byte_value)
    [print(m) for m in sorted_list]
    return sorted_list[-1]


if __name__ == '__main__':
    input_hex_string = '1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736'
    solution = main(input_hex_string)
    print("\nFor Set 1 Challenge 3, the best guess for the key is '%s' with a score of %f and a resulting message:"
          " %s" % (solution[2], solution[0], solution[1]))
    print('\n' + str(solution[1]))
