#!/usr/bin/python3

import base64


def hex_to_bytes(hex_string):
    return bytes.fromhex(hex_string)


def bytes_to_base64(bytes_value):
    return base64.b64encode(bytes_value).decode('utf-8')


def main(hex_string):
    return bytes_to_base64(hex_to_bytes(hex_string))


if __name__ == '__main__':
    input_hex_string = \
        '49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d'
    output_base64_string = 'SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t'

    if main(input_hex_string) == output_base64_string:
        print("Set 1 Challenge 1 solved!")
        print('\n' + str(hex_to_bytes(input_hex_string)))
    else:
        print("Wrong solution.")
